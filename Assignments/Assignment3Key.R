#T4LS19 Assignment3 Key

library(seqinr)

#########################################PartA##############################################
#Question1
#Data Portal:provides open access to dataset. usually raw data
#Phytozome
#IMG provides tools for comparative analysis of draft and complete microbial genomes 
#IMG/M is an extension of IMG; provides tools of analysis for metagonemic data

#Question2
#GOLD: Genomes OnLine Database; provides access to metagenomic for genomic sequencing prjoects
#Entry on the spreadsheet

#Question3: SCREENSHOT

#Question4
#1996, 5885 genes
#Taxonomy ID: 559292 (https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=559292)
#1st 10 Nucelotides on Chr4: ACACCACACC
#1st gene: COS7

#Question5
# Fungi -> Dikarya -> Ascomycota -> Saccharomycotina -> Candida albicans SC5314
# 6040 genes
#1st 10 Nuceleotides of Scaffold 1:  AAATAAGTTT
#Scaffold length: 2286237

#Question6
#FASTA:text-based format where single letters are used to represent either nucleotide or amino acid sequences

#loading the data, change path for your self
#CA<-read.fasta(file= "/Users/Sanny/Downloads/Ca.gz")

#990th Nucleotide on 3rd Scaffold: 
nucleo.990<- CA[["scaffold_3"]][990]
nucleo.990

########################################PartB#################################################
measurements <- c( 0.43, 44.88, 57.34, 17.12, 11.71, 26.12, 
                   28.36, 12.86, 22.80, 69.17, 33.02, 46.18, 
                   65.27, 30.55, 39.55, 33.62, 46.05, 93.01, 
                   52.58, 69.34 )

#Question1
mean<-mean(measurements)
mean

median<-median(measurements)
median

max.index<-which.max(measurements)
max.index

min.index<-which.min(measurements)
min.index

add.mean.centred<-c(measurements[]+median(measurements))
add.mean.centred

subtract.centred<-c(measurements[]-median(measurements))
subtract.centred

#Question2+3
#theres many ways to get the right answer
WatsonCrick <- function(InputStrand) {
  OutputStrand<-c()
  #here i am creatin an empty output vector
  InputStrand <- toupper(InputStrand)
  #here everything is being turned into upper case
  for (i in 1:nchar(InputStrand)){
    if(substr(InputStrand,i,i)=="A") OutputStrand<-paste("T",OutputStrand,sep="")
    if(substr(InputStrand,i,i)=="T") OutputStrand<-paste("A",OutputStrand,sep="")
    if(substr(InputStrand,i,i)=="G") OutputStrand<-paste("C",OutputStrand,sep="")
    if(substr(InputStrand,i,i)=="C") OutputStrand<-paste("G",OutputStrand,sep="")
  }
  tolower(OutputStrand)}
#here everything is being output as lowercase

WatsonCrick("ACctAgctCga")

#Question4
genetic.code <- c(
  TTT="F", TTC="F", TTA="L", TTG="L", TCT="S", TCC="S", TCA="S",
  TCG="S", TAT="Y", TAC="Y", TAA="Stop", TAG="Stop", TGT="C", TGC="C",
  TGA="Stop", TGG="W", CTT="L", CTC="L", CTA="L", CTG="L",CCT="P",
  CCC="P", CCA="P", CCG="P", CAT="H", CAC="H", CAA="Q", CAG="Q",
  CGT="R", CGC="R", CGA="R", CGG="R", ATT="I", ATC="I", ATA="I",
  ATG="M", ACT="T", ACC="T", ACA="T", ACG="T", AAT="N", AAC="N",
  AAA="K", AAG="K", AGT="S", AGC="S", AGA="R", AGG="R", GTT="V",
  GTC="V", GTA="V", GTG="V", GCT="A", GCC="A", GCA="A", GCG="A",
  GAT="D", GAC="D", GAA="E", GAG="E", GGT="G", GGC="G", GGA="G", GGG="G"
)

#there are multiple ways of doing this

Nuc2AA <- function(InputDNA) {
  otherLetters <- setdiff(letters, c("a", "c", "g", "t"))
  #here i am defining what is acceptable for input characters
  for (i in 1:nchar(InputDNA)){
    if(substr(InputDNA, i, i) %in% otherLetters){
      stop("Invalid Nucleic Acid! Only A, C, T, and G are acceptable Nucleic Acids.")
      #could also have used waning() here; the exact same way
    }
  }
  if (nchar(InputDNA)%%3 > 0){
    #here i am telling R that is the modulus to the DNA length is not 0 (ie not divisible by 3) then function will stop
    stop("DNA is not divisible by 3. Invalid Length.")
  }
  AminoAcids <- c()
  #here i am creating an empty vector for amino acids sequence
  for (i in seq(1, nchar(InputDNA), 3)){
    triplet <- toupper(substr(InputDNA, i, i+2))
    #here were are creating triplets from input DNA using the seq() function
    #everything should be in upper; to match our list
    AminoAcids <- c(AminoAcids, genetic.code[triplet][[1]])
    #here we are keeping the previous amino acid, and adding the next genetic.code[][[1]] returns the amino acid name
  }
  return (AminoAcids)
}

Nuc2AA("aatgctgatgattgccccatg")

##########################################PartC###########################################
patients <- list(
  
  list(
    list( first.name = "Jane", last.name = "Doe", middle.name = "Katherine" ), 
    id = 1142 , age = 78, tumor.size = 5, stage = "IIa", er.status = "+", her2.status = "-"
  ),
  
  list(
    list( first.name = "Lisa", last.name = "Smith", middle.name = "Marie" ), 
    id = 1001 , age = 48 , tumor.size = 3, stage = "I", er.status = "-", her2.status = "+"
  ),
  
  list(
    list( first.name = "Sarah", last.name = "Ann", middle.name = "Beckingworth" ), 
    id = 400 , age = 88, tumor.size = 7, stage = "IV", er.status = "-", her2.status = "-"
  )
)
#Question1
stage<-patients[[3]][['stage']]
stage

#Question2
subset.of.patients<-patients[-2]
subset.of.patients

#Question3
patients[[4]] <- list(list(first.name="Jilly", last.name = "Jupe", middle.name = "Julie" ), 
                      id = 5000 , age = 54, tumor.size = "NA", stage = "I", er.status = "+", her2.status = "+")

#Question4
new.patient<- list(list(list(first.name="Susie", last.name = "Sue", middle.name = "So" ), 
                  id = 100 , age = 45, tumor.size = 3, stage = "II", er.status = "-", her2.status = "-"))

patients<-append(new.patient, patients)
patients

#Question5
under.50<-function(input){
  my.list<- list()
  for (i in 1:length(input)){
    if((patients[[i]]$age)<50){
      my.list<-list(my.list,patients[[i]])}}
  return(my.list)}

under.50(patients)

younger.than.50<-patients[sapply(patients,function(x) x$age<50)]
younger.than.50

#Question6
sorted.patients <- patients[order(sapply(patients, "[[","id"))]
sorted.patients

sorted.patients2<-patients[order(sapply(patients,function(x) x$id))]
sorted.patients2
###################################THE END#############################

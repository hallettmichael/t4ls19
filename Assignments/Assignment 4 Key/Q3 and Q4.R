# Question 3
my.exprs <- data.frame(x = c(-4,-4,-4,-4,-4,-4,2,2,2,2,2,2), y = c(-1,0,1,2,3,4,-1,0,1,2,3,4))
start <- matrix(c(-1,2.5,-1,1,-1,-2), 3, byrow = TRUE)

my.clusters <- kmeans( my.exprs, centers = start, iter.max = 1)
my.clusters
## cluster means of (2,3) (-4,1.5) (2,0) given after first iteration
## iter.max set to 1 to track the step-by-step position of centroids.

start <- matrix(c(2,3,-4,1.5,2,0), 3, byrow = TRUE)

my.clusters <- kmeans( my.exprs, centers = start, iter.max = 1)
my.clusters
## no change in centroid so only first iteration was necessary.



# Question 4
my.exprs <- data.frame(x = c(-1.5,-1.5,-1.5,0,0,0,1,1,1,5,5,5), y = c(-1,1,3,-1,1,3,-1,1,3,-1,1,3))
start <- matrix(c(-4,1,5.5,1,6,1), 3, byrow = TRUE)

options(warn = 2)
my.clusters2 <- kmeans( my.exprs, centers = start, iter.max = 1)
my.clusters2
## Empty cluster exception is thrown. Do first iteration using K=2 and assume no movement for (6,1).

start <- matrix(c(-4,1,5.5,1), 2, byrow = TRUE)

my.clusters2 <- kmeans( my.exprs, centers = start, iter.max = 1)
my.clusters2
## cluster means of (-0.1666667,1) (5,1) and hypothetical (6,1) given after first iteration.
## c(6,1) still throws empty cluster exception so use K=2 again.

start <- matrix(c(-0.1666667,1,5,1), 2, byrow = TRUE)

my.clusters2 <- kmeans( my.exprs, centers = start, iter.max = 1)
my.clusters2
## no change in centroid so only first iteration was necessary.


my.exprs <- data.frame(x = c(-1.5,-1.5,-1.5,0,0,0,1,1,1,5,5,5), y = c(-1,1,3,-1,1,3,-1,1,3,-1,1,3))
start <- matrix(c(-4,1,5.5,1,6,1), 3, byrow = TRUE)

options(warn = 2)
my.clusters2 <- kmeans( my.exprs, centers = start, iter.max = 10)
my.clusters2
## When exception is ignored, third cluster at (6,1) is ignored 
## and same final centroid position of (-0.1666667,1) (5,1) given which do not change in future iterations.


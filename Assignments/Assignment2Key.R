#T4LS19 Assignment2 Key

#Question1
#a)TP53
#b)ENSEMBL
#c)29
#d)NP_001263625, NM_001276696.2
#e)CCDS73971.1
#f)standardization of gene annotations
#g)
#RefSeq:Curated NCBI reference sequences for a gene
#GenBank:database of all sequences submited to NCBI
#ENSEML: genome browser for vertebrate genomes; tools include BLAST, BLAT, BioMart and the Variant Effect Predictor (VEP)

#Question2
#there was multiple ways of answering this; this is my favourite example

Rec<- TRUE
#Reg<- FALSE 
#Change Reg for testing iv)
L1<- TRUE
L2<- TRUE
L3<- TRUE

L1<- FALSE
L2<- FALSE
L3<- FALSE

#i)
if(Rec & L1|L2|L3==TRUE){
  Active<-TRUE
} else {Active<-FALSE}
Active==TRUE

#ii)
if(Rec & L3==TRUE){
  Active<-TRUE
} else {Active<-FALSE}
Active==TRUE

#iii)
if(Rec & L1==TRUE){
  Active<-TRUE
}else Active<-FALSE
if((L2|L3)==TRUE) {
  Active<-FALSE
} else {Active<-TRUE}
Active==TRUE

#iv)
if(Reg==TRUE) {
  Rec<-FALSE
} else {Rec<-TRUE}
if((Rec & ((L1|L2)|L3))==TRUE) {
  Active<-TRUE
} else {Active<-FALSE}
Active==TRUE

#or

#i)
Active <- (Rec&L1)|(Rec&L2)|(Rec&L3)

#ii)
Active <- (Rec&L3)|((Rec&L1)&(Rec&L2)&(Rec&L3))

#iii)
Active <- (Rec&L1)&!L2&!L3

#iv)
Active <- ((Rec&L1)|(Rec&L2)|(Rec&L3))&!Reg

#Question3
#i)
0:999
#ii)
first_thousand<-(0:999)
exclude<-c(17,567,978)
new_thousand<-setdiff(first_thousand,exclude)
#or
exclude2<-c(18,568,979) #add one since we start from 0
new_thousand2<-first_thousand[-exclude2]
#iii)
even<-seq(0,1999,by=2)
#iv)
odd<-seq(1,2000,by=2)
#v)
sum.even<-sum(even)
#vi)
first_even<-rep(even,len=500)

#Question4

genetic.code <- c(
  TTT="F", TTC="F", TTA="L", TTG="L", TCT="S", TCC="S", TCA="S",
  TCG="S", TAT="Y", TAC="Y", TAA="Stop", TAG="Stop", TGT="C", TGC="C",
  TGA="Stop", TGG="W", CTT="L", CTC="L", CTA="L", CTG="L",CCT="P",
  CCC="P", CCA="P", CCG="P", CAT="H", CAC="H", CAA="Q", CAG="Q",
  CGT="R", CGC="R", CGA="R", CGG="R", ATT="I", ATC="I", ATA="I",
  ATG="M", ACT="T", ACC="T", ACA="T", ACG="T", AAT="N", AAC="N",
  AAA="K", AAG="K", AGT="S", AGC="S", AGA="R", AGG="R", GTT="V",
  GTC="V", GTA="V", GTG="V", GCT="A", GCC="A", GCA="A", GCG="A",
  GAT="D", GAC="D", GAA="E", GAG="E", GGT="G", GGC="G", GGA="G", GGG="G"
)

#There was multiple ways of doing this, the simples two being:

names(genetic.code)[genetic.code=="V"]

which(genetic.code=="V")

#more complicated functions were also accepted

Amino.Acid<- function(x) {
  for( i in 1:length(genetic.code)) {
    if (genetic.code[[i]] == x) {
      y<-1
      codons <- list()
      codons[y]<-names(genetic.code[i])
      y<-y+1
      print(names(genetic.code[i]))
    }
  }
}

Amino.Acid("V")

#Question5
#one possible solution

InputStrand<-"ACCTGACT"
OutputStrand<-""
for (i in 1:nchar(InputStrand)){
  if(substr(InputStrand,i,i)=="A") OutputStrand<-paste("T",OutputStrand,sep="")
  if(substr(InputStrand,i,i)=="T") OutputStrand<-paste("A",OutputStrand,sep="")
  if(substr(InputStrand,i,i)=="G") OutputStrand<-paste("C",OutputStrand,sep="")
  if(substr(InputStrand,i,i)=="C") OutputStrand<-paste("G",OutputStrand,sep="")
}

#THE END
              
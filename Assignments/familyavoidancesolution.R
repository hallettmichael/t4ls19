

scores <- matrix( data = c(-1, -2, -2, 2, -1, 0, 3, -1, -1, 3, 0, -1, 2, -2, -2, -1),
                  byrow=TRUE, nrow = 4, ncol = 4,
                  dimnames = list( c("A", "C", "G", "T"), c("A", "C", "G", "T") ) )
View(scores)

DNAHybridStrength <- function( X, Y ) {
  
  # 1
  if (nchar(X) != nchar(Y)) { stop("not equal length") }
  
  # 2
  X <- toupper(X); Y <- toupper(Y)
  
  # 3
  Xtmp <- unlist(strsplit(X, ""));  Ytmp <- unlist(strsplit(Y, ""))
  checkX <- c( Xtmp, Ytmp) %in% c("A", "C", "G", "T")
  
  # 4
  if (!all(checkX)) {  stop("not a nucleotide")  }
  
  # 5
  my.score <- 0
  for (i in 1:length(Xtmp)) {
    my.score <- my.score + scores[ Xtmp[i], Ytmp[i] ]
  }
  return(my.score)
}

# step 3 could be done in diferentways that doesn't use this string split function
# for example

for (i in 1:nchar(X)) {
  if (!(substr(X, i, i) %in% c("A", "C", "G", "T"))) { stop("nope") }
}
# then repeat for Y with the same for loop. do that in the lab.

# if you don't want to use the %in% operator, you could do that with an if statement

tmp <- substr(X, i, i)
if (! ((((tmp == "A") || (tmp =="C")) || (tmp == "G")) || (tmp =="T"))) {
  stop("nopey-ity-nope-nope")
}

# step 4 could be replace with a for or while loop. they can work on that in the lab

for(i in 1:nchar(checkX)){
  if checkX[i]!=c("A"|"T"|"C"|"G") {stop("nope")}
}

# try doing step 5 without the strsplit (and unlist). That is using the substr fucnction 
# with a for loop

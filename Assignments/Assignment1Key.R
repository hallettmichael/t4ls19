#T4LS19 Assignment1 Key

#PARTA
#Task1

Sys.Date()

#Task2
getRversion()

#Task3
y<-3
x<-5
temp<-y
y<-x
x<-temp
rm(temp)

#PARTC
#Question1
#I accecepted any reasonable suggestions for papers. A bibliography was not
#required for this assignment, however the paper itself did need to be properly 
#identified in the answer. This includes the basics like main author,
#date published,and the journal it was published in.

#Question2
#PubMed: literature citations and abstracts 
#PMC:a free access digital archive (repository) for full-text journal 
#articles and extension of NLM’s permanent print collection including
#materials that are not covered by PubMed. 
#There should be around 18K hits for PubMed and around 40K hits for PMC when searching for tp53

#Question3
#TP53 has been found in 181 Genomes.(https://www.ncbi.nlm.nih.gov/gene/7157/ortholog/?scope=117570) 
#Chromosome: 17 . Location: 17p13.1 (NC_000017.11)

#Question4
#Incomplete genomic data for organisms of interest
#Lack of a Definition of “genome”
#other reasonable answers were also accepted

#Question5
#HGNC: The HUGO Gene Nomenclature Committee is a committee of the Human Genome 
#Organisation that sets the standards for human gene nomenclature.
#HGNC Name: Tumor protein p53
#Other names: BCC7 , BMFS5 , LFS1 , P53 , TRP53 , TP53
#GeneID:11998
#Exons:12 exons 
#Obervations: changes in # number of amino acids --> isoforms

#Question6
#The ancestor of a p53 gene is over a billion years. p53 itself is ~440MYA. 
#Found in mammals, birds, reptiles, amphibians, coelacanths, and bony fish. 

#Question7
#GI:associated to each new/unique NCBI sequence,
#RefSeq:2 letter sequence followed by an underscore and a number of digits 
#depending on type of data( e.g. genetic, protein, etc.)

#Question8
#GenBank is an open access database for nucelotide sequences and their protein products.
#in comparison NCBI is a curated database. 

#Question9 - I accepeted any four reasonable answers

#Question10
#there are 15 entries for tp53 proteins in humans, they are isoforms of one another

#Questions11
#Position 55: phosphorylated threonine (phosphothreonine).

#Question12
# Mainly: Tumour suppression and genomic stability. (but theres many functions)

#Question13
#ddSNP:large public database for SNPs and small scale (<50bp)  human genomic structural variants
#dbVar:databse for larger (>50bp) human genomic structural variants (e.g. translocations)
#ClinVar:public record of reports; relationships among human variations and their phenotype 

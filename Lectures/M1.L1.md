BIOL 480/510/630 Bioinformatics
========================================================
author: M Hallett, Department of Biology, Concordia University
date: September 2019
autosize:true
font-family: 'Helvetica' 
#output: beamer-presentation
#title: "Sample Document"
#output: pdf_document

## Tools for the Life Sciences
Module 1, Lecture 1

Logistics
========================================================

[mikehallett.science](https://mikehallett.science) My lab website<br>
[https://mikehallett.science/t4ls](https://mikehallett.science/courses/t4ls/) Course Website


${\tt michael.hallett@concordia.ca}$ e-mail<br>
${\tt @hallettmichael}$ Twitter<br>
${\tt michael.hallett}$ Slack



Some advice
========================================================

* Bioinformatics is an experimental science.
* This means that you need to  do experiments.
* The primary means of doing experiments in bioinformatics is through the use of statistics and computation.
* This requires programming.
* Learning to program is like learning a language.
* It is an investment, and I will motivate why this investment will change your career.
* Practice and all is coming (Sri K Pattabhi Jois).



Segue: All Biologist will be Computational Biologist
========================================================

* Consider what life science research looked like  circa 1990 (28 years ago).
* At that time, the vast majority of basic life sci researchers studied a single gene (or gene product), or at most a single complex (e.g. ribosome).
* In the '90s, How did an ESR (Estrogen Receptor) researcher "track" new results?
* Internet popularized in ~'92. PubMed released in '96. GenBank '88. dbGap '07.
* A lot of actual walking to a library and looking up keywords at the back of a journal that seemed likely to publish results about ESR.
* That is, it was the dark ages. 

Segue (2): And the Geeks shall inherit the earth
========================================================

* Then came the internet in '92. General interconnectivity. Within 5 years, every major journal was publishing their papers on-line.
* Then came PubMed.
* The ability to search text for keywords (eg ESR, estrogen) within english text allowed single genes/gene products to be followed closely.
* So Pubmed allowed researchers to identify papers that mentioned a gene (eg ESR). A lot of Principle Investigators (PIs) still primarily and only use PubMed to track their genes.


Information Technology (IT) Skills
========================================================

* We will be using a suite of technologies throughout the course.<br>

* In fact, all communication, assignments and file sharing will be done using these
tools.<br>

* This is a good investment since these tools are now commonly used in both academic and industrial
workplaces.


IT Skills: Cloud-based file sharing
========================================================

Cloud-based file sharing and multi-person project development

Google Drive [drive.google.com](http://drive.google.com) <br>

How are Drive-like cloud-based approaches different from traditional methods for multi-person project development?

(Question 1 of Assignment 1 will ask that you get a gmail email account, if you do not have one, and
develop a directory/files on Google Drive.)

IT Skills: Cloud-based file sharing
========================================================
<img src="MyFigs/cloud1.jpg" alt="SysBioLogo" style="width: 1000px;"/>

IT Skills: Cloud-based file sharing
========================================================
<img src="MyFigs/cloud2.jpg" alt="SysBioLogo" style="width: 1000px;"/>

IT Skills: Cloud-based file sharing
========================================================
<img src="MyFigs/cloud3.jpg" alt="SysBioLogo" style="width: 1000px;"/>


IT Skills: Commmunicatoin/messaging systems
========================================================

Cloud-based approaches for **persistent** message and information sharing.

Slack [https://slack.com/](https://slack.com/) <br>

There are competitors. For example, the open source system [https://www.mattermost.org/](https://www.mattermost.org/)

(Assignment Question 1.1 asks that you get a slack account and join the #COMP480_2019 room in the Concordia Bioinfomratics workspace.)

<img src="MyFigs/slack.png" alt="SysBioLogo" style="width: 2000px;"/>

IT Skills: Cloud-based Approaches
========================================================

What are the drawbacks?

IT Skills: Cloud-based Project Management
========================================================

Cloud-based approaches that allow multiple people to track progress in a project.

Trello [https://trello.com/](https://trello.com/) <br>

<img src="MyFigs/trello.png" alt="SysBioLogo" style="width: 2000px;"/>



IT Skills: Software Development Platforms
========================================================

Hybrid of cloud-based and local approaches that facilitates the development of software by multiple persons. 

In particular, the software remains **consistent**: each person can make changes to the software but the system ensures tat everyone has access to the same version).

The software is stored **persistently**: each person can view previous, earlier versions of the software and develop their own "branches" of the system.

GitHub [https://github.com/](https://github.com/) <br>

BitBucket [https://bitbucket.org/](https://bitbucket.org/) <br>

<img src="MyFigs/bb.png" alt="SysBioLogo" style="width: 2000px;"/>



IT Skills: Software Development Platforms
========================================================


BitBucket [https://bitbucket.org/](https://bitbucket.org/) <br>

* We will be using BitBucket (BB) throughout this course, and we will use introductory features of a tool called GIT that guarantees the consistency and persistency of shared files.

<img src="MyFigs/bb.png" alt="SysBioLogo" style="width: 2000px;"/>


IT Skills: Modern Web Development
========================================================

Both cloud-based and local approaches for fast and fancy web site development.

Lab website [https://mikehallett.science](https://mikehallett.science)<br>

<img src="MyFigs/mh.sci.png" alt="SysBioLogo" style="width: 1000px;"/>


IT Skills: Modern Web Development
========================================================

I developed my website from an open source website of a colleague.

<img src="MyFigs/bedford.png" alt="SysBioLogo" style="width: 1000px;"/>

IT Skills: Modern Web Development
========================================================

The source code for both my website and my colleague (Bedford) are publicly availalbe through GitHub.

(Recall that GitHub is like BitBucket.)

<img src="MyFigs/bedford.gh.png" alt="SysBioLogo" style="width: 700px;"/>
<img src="MyFigs/hallett.gh.png" alt="SysBioLogo" style="width: 500px;"/>
<img src="MyFigs/hallett.web.gh.png" alt="SysBioLogo" style="width: 500px;"/>

IT Skills: Developer communities
========================================================

Any sort of IT, technical or programming questions that you have have probably been answered already on Stack Overflow. 

Stack Overflow  [https://stackoverflow.com/](https://stackoverflow.com/) <br>

<img src="MyFigs/stackoverflow.png" alt="SysBioLogo" style="width: 1000px;"/>

IT Skills: Laboratory Management
========================================================

Tools such as Quartzy  [https://www.quartzy.com/](https://www.quartzy.com/) help
to assist bio-labs purchase, catalog, and track laboratory equipment, reagents, and general stock.

<img src="MyFigs/quartzy.png" alt="SysBioLogo" style="width: 8000px;"/>

IT Skills: Literature Management
========================================================

This is primarily for academic and industrial scientists.

Tools such as Zotero  [https:/zotero.org/](https://www.zotero.org) help
you organize papers and literature references. 
As you find papers of interest, you can put them into this electronic library and access
them anywhere (they papers are in the cloud).
Then, if you are writing a paper, it is easy to generate bibliographies.
Everyone in your group can synchronize with the library and access the papers also.

<img src="MyFigs/zotero.png" alt="SysBioLogo" style="width: 2000px;"/>


IT Skills: Integration between devices and sites
========================================================

This is a cloud-based tool that allows you to link one of your devices or web site with other sites.

**The "Internet of Things"**

Devices include smart phones, computers, cars, appliances, lab machinary. There are a long list of sites including Drive, Slack, GitHub etc.

If this then that (IFTTT) [https://ifttt.com/](https://ifttt.com/) <br>


IT Skills: Action Items (in preparation for Assignment 1)
========================================================

1. Get a Google gmail account, if you don't have one. (Since you're going to use this for the course, it would be great if  Makayla and I can tell immediately whoit account belongs to. E.g. King@gmail.com is a bit vague.) 

2. Get an account for Slack, Trello, IFTTT, Quartyz and Mendeley. (It might be wise to have a password system that is easy to remember, and to link all of these objects to your gmail account.)

3. Go to the following Google Doc and enter your name, gmail account and Slack name. Becuase of the way permissions work with Google Drive, it is important that you give a gmail account. We will use this to send invitations for BitBucket and to the workspace within Slack.

https://docs.google.com/document/d/1DDV3Azup1wr4Rc76OH8aFIouQU7WzhsyIwh46zw6FuQ/edit?usp=sharing 



BIOL480 (c) M Hallett, CB Concordia
========================================================

<img src="MyFigs/sysbiologo.png" alt="SysBioLogo" style="width: 500px;"/>


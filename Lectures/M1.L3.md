Module 1, Lecture 3: IT Skills - R and R Studio
========================================================
author: M Hallett
date: September 2019
autosize:true
font-family: 'Helvetica' 
#output: beamer-presentation 

## R, RStudio for the course.



The R Project (www.r-project.org)
========================================================

![R homepage](MyFigs/R.homepage.png)

The R Project
========================================================
### **What is R?**

* R is a language and environment for statistical computing and graphics.
* It is a GNU project which is similar to the S language developed at Bell Laboratories by Jo
hn Chambers et al.

* R and it's assocaited packages togetherprovide a wide variety of functionalties:
  + linear and non-linear modelling,
  + classical statistical tests,
  + time-series,
  + classification,
  + clustering, graphics, .... and it is highly extensible.

<br><br><hr><br>

Installing R on your  machine
========================================================


Available @ CRAN (Comprehensive R Archive Network) Vers 3.1.2
<tiny><code><font color = "blue">
R version 3.1.2 (2014-10-31) -- "Pumpkin Helmet"<br>
Copyright (C) 2014 The R Foundation for Statistical Computing<br>
Platform: x86_64-apple-darwin10.8.0 (64-bit)<br>
<br>
R is free software and comes with ABSOLUTELY NO WARRANTY.<br>
You are welcome to redistribute it under certain conditions.<br>
Type 'license()' or 'licence()' for distribution details.<br>
<br>
Type 'demo()' for some demos, 'help()' for on-line help, or<br>
'help.start()' for an HTML browser interface to help.<br>
Type 'q()' to quit R.<br>
\> <br>
</font color>
</code></tiny>


Manuals and other learning aids
========================================================

* Manual @ http://www.r-project.org/
* On-line course @ http://tryr.codeschool.com/
* Additional items at course homepage...
* https://mikehallett.science
* <code><font color="blue">Stackoverflow.com </font color></code> is really good. Most common questions have aleady been asked and answered.


RStudio: tools that make R easier to use
========================================================

* Free for academic use.
* http://www.rstudio.com/

<img src="MyFigs/Rstudio.screenshot.png" alt="RStudio" style="width: 2000px;"/>


RStudio
========================================================

...
* 4 main windows
  + top left: your code
  + bottom left: an R session where you can run your code
  + top right: details of your R environment (defined variables, datastructures, history, ...
)
  + bottom right: input/output (access to your files, plots that you produce, help, ...)
  
***

<img src="MyFigs/Rstudio.screenshot.png" alt="RStudio" style="width: 1250px;"/>

Starting with RStudio (1)
========================================================


<tiny>
<code><font color="blue">
* Preferences/General Default Working Directory<br>
* Preferences/Sweave <font color="black"> under Weave Rnw files type</font> knitr<br>
* Tools/Install Packages <font color="black"> under Packages type</font> knitr<br>
-- <font color="black"> then type y in lower left window when instructed</font><br>
</font>
</code>
</tiny>

The knitr package will allow you to look at the lecture notes a bit more easily.

Creating a directory for your repositories
========================================================

* On Macs or Linux, open a <code><font color="blue"> terminal </font></code> window.<br>
-- This is in the <code><font color="blue">Applications/Utilities </font></code> folder.<br>
* <code><font color="blue"> cd ~ </font></code><br>
-- This puts you in your "home" directory. ("above" your Desktop actually)
* <code><font color="blue"> mkdir repo </font></code><br>
-- This makes a directory for the course repository.
* <code><font color="blue"> cd repo </font></code><br>
-- cd (change directory) into repo<br>


A couple of operating system things (2)
========================================================
* Go to BitBuck and find the page for T4LS19.<br>
* Look for the <code><font color="blue"> clone </font></code> option.<br>
-- This is the instruction you need to copy and paste into your repo directory.<br>
* Copy the <code><font color="blue"> git clone https:... </font></code><br>
* Paste this code into your terminal window. <br> 
-- (So in the <code><font color="blue"> repo </font></code> directory.)
* You *might* get an error as your machine may not have git (properly) installed.



Installing or Fixing Git on a Mac (1)
========================================================

* On a modern, updated Mac with El Capitan, you might have to update some of the developer code. The instructions are on the course web page.
* On older Macs or PCs, we will have to deal with it on a case-by-case basis.
* On PCs, you can download Git here: 
https://git-scm.com/downloads
* Go back and do the <code><font color="blue">git clone ...</font color></code>  after it is installed.


After a successful cloning (3)
========================================================


In your <code><font color="blue">terminal</font color></code> window type: <br>
<code><font color="blue">ls</font color></code><br>
total 8<br>
drwxr-xr-x 8 hallett nogroup 5 Jan 15 14:10 T4LS19<br>

<font color="red">_So there is now a director in repo._</font>
<code><font color="blue">cd T4LS19</font color></code><br>
<font color="red">_Change directory (cd) into the T4LS19 directory that you just cloned._</font color>

What the repository looks like
========================================================

<code><font color="blue">ls</font color></code><br>
total 17<br>
-rw-r--r-- 1 hallett nogroup 91 Jan 12 14:10 README<br>
drwxr-xr-x 2 hallett nogroup  3 Jan 12 14:10 assignments<br>
drwxr-xr-x 2 hallett nogroup  8 Jan 12 14:10 data<br>
drwxr-xr-x 2 hallett nogroup  3 Jan 12 14:10 experiments<br>
drwxr-xr-x 2 hallett nogroup  3 Jan 12 14:10 lectures<br>
drwxr-xr-x 2 hallett nogroup  4 Jan 12 14:10 src<br>

<font color="red">_Change directory (cd) into the T4LS19 directory that you just cloned._</font color>


What the repository looks like (2)
========================================================
<code><font color="blue">cat README</font color></code><br>
...

<font color="red">_The cat (concatenate) commands allows you to look at the contents of a text file._</font color>

<font color="red">_README files are sort of standard
for providing users in Unix with important information about files._</font color>

What the repository looks like (3)
========================================================

<code><font color="blue">cd lectures</font color></code><br>
<code><font color="blue">ls -l</font color></code><br>


<font color="red">_But it's easier to examine these files using the bottom-right RStudio window (Files)._</font color>

<img src="MyFigs/Rstudio.screenshot.png" alt="RStudio" style="width: 1500px;"/>


Creating a Project within RStudio
========================================================

* To make the connection between your repository and your clone of it, first make an RStudio project.
* In RStudio, choose <code><font color="blue">File/New Project/Existing Directory/Browse</font color></code>.
* Now select your cloned directory <code><font color="blue">T4LS19</font color></code>.
* In the Top-Right RStudio window, select <code><font color="blue">Git</font color></code>.
* Any files you change, will be highlighted. 
* If you select these files, and then hit <code><font color="blue">Commit</font color></code>, your files will be securely saved.





"Pulling" updates of the course files via GIT (1)
========================================================

The nice thing about using GIT is that it allows us to smoothly update, fix and modify the course notes, data and R scripts.

### Step 1 ###
* Initiate an RStudio session using the rstudio.cs.mcgill.ca as above.

### Step 2 (simple)###

* Assuming you have cloned the  directory and you have made an Rproj (see above), select the Top-Right RStudio window.
* Choose the <code><font color="blue">Git</font color></code> panel and hit the <code><font color="blue">Pull</font color></code> button.

"Pulling" updates of the course files via GIT (2)
========================================================

### Step 2 (more complicated)###
* Go to <code><font color="blue">Tools/Shell</font color></code>.
* <code><font color="blue">cd ~/T4LS19</font color></code> or wherever you created your T4L18 directory (eg ~/repo/T4LS19).
*  <code><font color="blue">git pull</font color></code>

In other words, cd (change directory) to the place where you initially cloned the student T4LS19 directory (the previous slides suggest ~/T4LS19). The pull commands "pulls" all the updates, modifications and additions that Dani or I have made and merges them with your file, seemlesssly.


 Problems 
========================================================
* RStudio: Kill it and re-start. Worst case, re-install & re-create project.
* GIT problems: ask. 
* R should be ok but sometimes you might have problems with missing libraries and packages. Ask.


 Action Items 
========================================================
* Install R on your machine
* Install RStudio on your machine
* Clone the T4LS19 repository at BitBucket. It should be located at ~/repo/T4LS19
* Create a project called T4LS19 using the directory ~/repo/T4LS19 in RStudio.
* Look at the slides for Module 1, lectures 1, 2, and 3. (Remember to activate knitr; see above)


## Basics of Unix



Some Basic Unix  (not for PCs!)
========================================================

* Log into your account on your Unix/Linux machine or on a Mac laptop/desktop.
* Open (double click) on Applications/Utilities/Terminal

![terminal window](MyFigs/terminal.window.png)


<code><font color="blue">cd</font color></code><br>
<font color="red">_cd = "change directory". When you type this alone, you are sent back to your home._</font color>

<code><font color="blue">cd \~</font color></code><br>
<font color="red">_The tilde is a symbol meaning your home. Executing "cd" and "cd \~" are the same._</font color>

Some Basic Unix  (not for PCs!)
========================================================

<code><font color="blue">ls</font color></code><br>
<font color="red">_"ls = list. This lists all the files & directories in your currently location._</font color>

![terminal window2](MyFigs/terminal.window2.png)

Some Basic Unix
========================================================

<code><font color="blue">cd repo</font color></code><br>
<font color="red">_This moves you down one level of the tree into a directory called repo that's on my machine._</font color><br>
<code><font color="blue">ls</font color></code><br>

![terminal window3](MyFigs/terminal.window3.png)

Some Basic Unix
========================================================
<code><font color="blue">ls -l</font color></code><br>

![terminal window4](MyFigs/terminal.window4.png)

<font color="red">_-l is called a flag. Flags are way to communicate additional information to Unix commands. Here the -l flag for  the ls command tells us which are files and which are directories, permissions (more later), who owns it, size, when it was last modified etc._</font color>

Some Basic Unix
========================================================
<code><font color="blue">cd T4LS19; ls -l</font color></code><br>
![terminal window5](MyFigs/terminal.window5.png)
<font color="red">_First I went "down" one more level into the directory called repo. The semicolon ; separtes Unix commands oso you can put multiple commands on the same line._</font color>


Some Basic Unix
========================================================
<code><font color="blue">clear; pwd</font color></code>
<font color="red">_(clear. It clears the screen._</font color>
<font color="red">_print working directory. This tells you where you are in your tree.)_</font color><br>
<code><font color="blue">cd ..</font color></code>
<font color="red">_ (This .. means go up the tree one level.)_</font color> <br>
<code><font color="blue">pwd; cd ../..</font color></code>
<font color="red">_(The / is the symbol for a directory. So this commands says "go up two levels". _</font color><br>
<code><font color="blue">pwd; ls</font color></code><br>
<code><font color="blue">ls michaelhallett</font color></code><br>
<font color="red">_So here I list the contents of the directory but don't change my working directory._</font color><br>
<code><font color="blue">pwd</font color></code> <font color="red">_Btw michaelhallett is my username on the machine._</font color><br>


![terminal window6](MyFigs/terminal.window6.png)


Some Basic Unix
========================================================

<code><font color="blue">cd ~; pwd</font color></code>
<font color="red">_(Back home!)_</font color><br>
<code><font color="blue">cd repo/T4LS19/Lectures; ls</font color></code><br>

![terminal window.7](MyFigs/terminal.window.7.png)<br>
<font color="red">_These are the lecture notes for the course. Notice the different file extensions._</font color><br>


Some Basic Unix
========================================================
cat onco.semic*

![terminal window9](MyFigs/terminal.window9.png)<br>
<font color="red">_cat = concatenate. It prints the file to the screen (which is usually referred to as "standard output or std_out)._</font color><br>
<font color="red">_The * is a wildcard character. It tells Unix to find *all* files that start with "onco.semic". In this case there is only one. Consider what would happen if I instead used "cat M1.L*.html"_</font color><br>

Some Basic Unix
========================================================

<code><font color="blue">touch my.new.lecture</font color></code><br>
<code><font color="blue">ls -l</font color></code><br>
<code><font color="blue">cat my.new.lecture</font color></code><br>
![terminal window10](MyFigs/terminal.window10.png)<br>

<font color="red">_touch creates an empty file with the given name._</font color>

Some Basic Unix
========================================================

<code><font color="blue">mv my.new.lecture \~/my.new.lecture.moved</font color></code><br>
<font color="red">_mv = move. This moves the file "my.new.lecture" to a new location (my home directory \~) and a new name "my.new.lecture.moved"._</font color><br>
<code><font color="blue">ls -l ~</font color></code><br>
![terminal window11](MyFigs/terminal.window11.png)<br>


Some Basic Unix
========================================================

<code><font color="blue">cp ~/my.new.lecture.moved new.copy.old.location</font color></code><br>
<font color="red">_cp=copy. It makes a copy (but doesn't destroy the original) of a file. The second argument is the name of the file. You should verify that the original file in ~ is still there. _</font color>


Some Basic Unix
========================================================

<code><font color="blue">rm ~/my.new.lecture.moved</font color></code><br>
<font color="red">_rm=remove. This removes the specified file. Unix might prompt you to be sure._</font color><br>
<code><font color="blue">ls ~</font color></code><br>
![terminal window12](MyFigs/terminal.window12.png)<br>


Some Basic Unix
========================================================

<code><font color="blue">cd ~/repo/T4LS19; cp -r ~/repo/T4LS19/ course.backup</font color></code><br>

<font color="red">_The -r flag means "recursive": Copy c all the files/directories within T4LS19 and all the files/directories within each directory within T4LS19, etc. etc. etc. to a new location called "course.backup"_</font color>

<code><font color="blue">ls -l</font color></code><br>
![terminal window13](MyFigs/terminal.window13.png)<br>


Some Basic Unix
========================================================

<code><font color="blue">rm -r course.backup</font color></code><br>

<font color="red">_The -r flag again means "recursive". but this will take a long time..._</font color>

<code><font color="blue">rm -r -f course.backup</font color></code><br>
<font color="red">_The -f flag again means force the remove (don't ask for permission)._</font color>

Some Basic Unix
========================================================

<code><font color="blue">mkdir my.new.directory</font color></code><br>
<code><font color="blue">ls</font color></code><br>
<code><font color="blue">cd my.new.directory</font color></code><br>
<code><font color="blue">touch my.new.file</font color></code><br>
<code><font color="blue">ls</font color></code><br>
<code><font color="blue">cd ..</font color></code><br>
<code><font color="blue">rmdir my.new.directory</font color></code><br>
<code><font color="blue">rm -r my.new.directory</font color></code><br>

![terminal window15](MyFigs/terminal.window15.png)<br>

<font color="red">_mkdir = make directory. touch = make new file._</font color>


Some Basic Unix (11)
========================================================
<code><font color="blue">top</font color></code><br>

![terminal window14](MyFigs/terminal.window14.png)<br>

             
<font color="red">_All the processes running on the machine. _</font color>

Action Items
========================================================

Using a Mac or Linux machine, try out the above examples.


BIOL 480 Bioinforamtics (c) M Hallett, CB-Concordia
========================================================
![CB Concordia](MyFigs/sysbiologo.png)



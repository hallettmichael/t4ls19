\documentclass{article}\usepackage[]{graphicx}\usepackage[]{color}
% maxwidth is the original width if it is less than linewidth
% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%
\let\hlipl\hlkwb

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}
\usepackage{fancyhdr}
\usepackage[margin=2.0cm]{geometry}%rounded up from 1.87, just to be safe...
\usepackage{parskip}
\usepackage{float}
%\usepackage{times} %make sure that the times new roman is used
\usepackage{mathptmx}
\usepackage{amsmath}

\usepackage{blindtext}
\title{Module 2, Lecture 4: Supervised Learning via Regression.}
\date{November 2019}
\author{Tools for the Life Sciences\\ MT Hallett\\ Concordia University}


%      ------ Format Stuff ---------
\newlength{\itemdist}
\setlength{\itemdist}{0.05ex}
\newlength{\headdist}
\setlength{\headdist}{0.04ex}

\newcommand{\R}{\mathbb{R}}
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}
%\SweaveOpts{concordance=TRUE}
%\SweaveOpts{concordance=TRUE}
%\pagestyle{fancy}

\maketitle
Linear regression can be used as a supervised machine learning technique  to predict the value
of a continuous outcome variable $y$ based on attributes $x$.
Here $y$ is typically a real number.
This is in contrast to our previous lecture (M2,L2) where we discussed
classification,  a supervised form of machine learning but where $y$
has a (finite) number of categorical (discrete) ``levels'' (eg good/bad, yes/no, Monday-Sunday).
The goal here is to model the continuous variable $Y$ as a function of
one or more attributes of a variable $X$. 
This function is a line, and it is determined using a learning
set as before
\[ 
 X = 
 \begin{matrix}
  x[1, 1] &  x[1, 2] & \dots & x[1, D] \\
  x[2, 1] &  x[2, 2] & \dots & x[2, D] \\
  \vdots & \vdots  & \vdots & \vdots \\
  x[N, 1] &  x[B, 2] & \dots & x[N, D] \\
 \end{matrix}
\]
and $Y = [ y[1], y[], \ldots, y[N] ]$. So here, $y[i]$ is a real number 
and corresponds to the instance $x[i, ]$.
We use $X$ and $Y$ to infer the linear relationship as best possible 
(or decide that no such relationship exists).

Assuming that we conclude that $Y$ is a linear function of $X$,
we can use the line to predict the value of $Y$ on future ``unseen'' examples of $X$.

If $D =1$, at least part of the model should be very familiar from grade school algebra:
$$ y[i] =  m \cdot x[i,1] + b + \epsilon$$
We commonly refer to $b$ as the $y$-intercept and $m$ as the slope.
In regression, we typically do not use $b$ and $m$ though for various reasons.
We will write instead
$$ y[i] = \Theta_0 + \Theta_1 \cdot x[i,1] + \epsilon.$$
(Our notation and the order has changed.)
Here $\epsilon$ requires some explanation. 
In our model, we do not expect each $y[i]$ to be ``exactly'' a function
of the line $m \cdot x[i,1] + b$ but show be ``close'' to the line.
Here $\epsilon$ is called a random variable and it allows for a little
bit of ``noise'' to jiggle a point above or below the line.
Often, $\epsilon$ would be modeled as a random variable that follows
the standard normal distribution with mean $0$ and standard deviation $1$.
Since $\epsilon$ adds a bit of randomness to our model, the outcome 
$y[i]$ is also (somewhat) random (it is a line plus some jiggle).
For this reason we write,
$$ y[i] \sim \Theta_0 + \Theta_1 \cdot x[i,1] + \epsilon.$$
(We read the $\sim$ to mean ``is distributed according to''.)

In R, you can explore the  normal distribution easily.
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{epsilons} \hlkwb{<-} \hlkwd{rnorm}\hlstd{(} \hlkwc{n}\hlstd{=}\hlnum{1000}\hlstd{,} \hlkwc{mean}\hlstd{=}\hlnum{0}\hlstd{,} \hlkwc{sd} \hlstd{=} \hlnum{1}\hlstd{)} \hlcom{# generate 100 examples of this distribution.}
\hlkwd{head}\hlstd{(epsilons)}
\end{alltt}
\begin{verbatim}
## [1]  0.07891032 -1.00451794  0.81401742  0.69604564  0.71470503  0.87728901
\end{verbatim}
\begin{alltt}
\hlkwd{summary}\hlstd{(epsilons)} \hlcom{# some are positive and some negative  }
\end{alltt}
\begin{verbatim}
##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
## -2.74432 -0.73354 -0.02539 -0.01217  0.67448  3.41206
\end{verbatim}
\begin{alltt}
\hlkwd{library}\hlstd{(ggplot2)}
\hlkwd{qplot}\hlstd{( epsilons,} \hlkwc{main}\hlstd{=}\hlstr{"Figure. These 1000 epsilons are each point to a little jiggle to the line"} \hlstd{)}
\end{alltt}


{\ttfamily\noindent\itshape\color{messagecolor}{\#\# `stat\_bin()` using `bins = 30`. Pick better value with `binwidth`.}}\end{kframe}
\includegraphics[width=\maxwidth]{figure/unnamed-chunk-1-1} 

\end{knitrout}

\newpage

\section{The Simple Model}

In this section, we consider a very simple linear model that has only 
attribute to predict the value of $y$. 
This involves just two parameters to describe the line:
two {\em parameters} $\Theta_0$ (the $y$-intercept) and $\Theta_1$ (the slope
of the line).

\subsection{The learning dataset}
Let's investigate if there is a linear relationship between KLF15 (Krupple-like Factor 15) and time to recurrence, using the TCGA RNA-sequencing data that you are already familiar with. This is a transcription facotr that 
has already been implicated in human disease (glomerular and muscle hyptertrophy). You can read more about KLF15 here at a very nice database called
GeneCards {\tt https://www.genecards.org/cgi-bin/carddisp.pl?gene=KLF15}.

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{load}\hlstd{(}\hlstr{"~/repos/t4ls19/data/tcga.RDATA"}\hlstd{)}
\hlstd{klf15} \hlkwb{<-} \hlkwd{which}\hlstd{(tcga}\hlopt{$}\hlstd{probe.info}\hlopt{$}\hlstd{gene.name} \hlopt{==} \hlstr{"KLF15"}\hlstd{)[}\hlnum{1}\hlstd{]}
\end{alltt}
\end{kframe}
\end{knitrout}
Although there are several transcripts for KLF15, we will investigate
the first one in more depth (index $9966$).

We start by selecting only patients from the TCGA dataset that recurred
within the first five years.
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{rclinical} \hlkwb{<-} \hlstd{tcga}\hlopt{$}\hlstd{clinical[}\hlkwd{which}\hlstd{(tcga}\hlopt{$}\hlstd{clinical}\hlopt{$}\hlstd{event.5}\hlopt{==}\hlnum{TRUE}\hlstd{), ]}
\hlkwd{nrow}\hlstd{(rclinical)}
\end{alltt}
\begin{verbatim}
## [1] 41
\end{verbatim}
\end{kframe}
\end{knitrout}
Now remove the corresponding columns from the expression matrix
and visualize.
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{rexprs} \hlkwb{<-} \hlstd{tcga}\hlopt{$}\hlstd{exprs[ ,} \hlkwd{which}\hlstd{(tcga}\hlopt{$}\hlstd{clinical}\hlopt{$}\hlstd{event.5)]}
\hlkwd{dim}\hlstd{(rexprs)}
\end{alltt}
\begin{verbatim}
## [1] 90797    41
\end{verbatim}
\begin{alltt}
\hlstd{df} \hlkwb{<-} \hlkwd{data.frame}\hlstd{(}  \hlkwc{gene} \hlstd{= rexprs[klf15, ],} \hlkwc{time} \hlstd{= rclinical}\hlopt{$}\hlstd{time.5)}
\end{alltt}
\end{kframe}
\end{knitrout}
So it does seem that increased KLF15 expression is linearly related to time to recurrence. 
Here patients with low levels of KLF15 recur earlier than patients with high levels of KLF15.
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{qplot}\hlstd{(gene, time,} \hlkwc{data}\hlstd{=df)}
\end{alltt}
\end{kframe}

{\centering \includegraphics[width=5in,height=4in]{figure/unnamed-chunk-5-1} 

}



\end{knitrout}

It's not a perfect linear relationship.
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{qplot}\hlstd{(gene, time,}  \hlkwc{data}\hlstd{=df,}\hlkwc{geom} \hlstd{=} \hlkwd{c}\hlstd{(}\hlstr{"point"}\hlstd{,} \hlstr{"smooth"}\hlstd{))}
\end{alltt}


{\ttfamily\noindent\itshape\color{messagecolor}{\#\# `geom\_smooth()` using method = 'loess' and formula 'y \textasciitilde{} x'}}\end{kframe}

{\centering \includegraphics[width=5in,height=4in]{figure/unnamed-chunk-6-1} 

}



\end{knitrout}
But close enough that we consider using a linear model.

\subsection{Building a simple linear model}

We want to draw a line from 0 to 60 months ($x$-axis is time to recurrence).
The line that we draw should minimize the distance of each point to the line.
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{sp} \hlkwb{<-} \hlkwd{ggplot}\hlstd{(}\hlkwc{data}\hlstd{=df,} \hlkwd{aes}\hlstd{(}\hlkwc{x}\hlstd{=gene,} \hlkwc{y}\hlstd{=time))} \hlopt{+} \hlkwd{geom_point}\hlstd{()}
\hlstd{sp} \hlopt{+} \hlkwd{geom_abline}\hlstd{(}\hlkwc{intercept} \hlstd{=} \hlnum{20}\hlstd{,} \hlkwc{slope} \hlstd{=} \hlnum{15}\hlstd{,} \hlkwc{color}\hlstd{=}\hlstr{"red"}\hlstd{,} \hlkwc{size} \hlstd{=} \hlnum{1}\hlstd{)}\hlopt{+}
  \hlkwd{ggtitle}\hlstd{(}\hlstr{"too shallow"}\hlstd{)}
\end{alltt}
\end{kframe}

{\centering \includegraphics[width=5in,height=4in]{figure/unnamed-chunk-7-1} 

}



\end{knitrout}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{sp} \hlkwb{<-} \hlkwd{ggplot}\hlstd{(}\hlkwc{data}\hlstd{=df,} \hlkwd{aes}\hlstd{(}\hlkwc{x}\hlstd{=gene,} \hlkwc{y}\hlstd{=time))} \hlopt{+} \hlkwd{geom_point}\hlstd{()}
\hlstd{sp} \hlopt{+} \hlkwd{geom_abline}\hlstd{(}\hlkwc{intercept} \hlstd{=} \hlnum{40}\hlstd{,} \hlkwc{slope} \hlstd{=} \hlnum{110}\hlstd{,} \hlkwc{color}\hlstd{=}\hlstr{"red"}\hlstd{,} \hlkwc{size} \hlstd{=} \hlnum{1}\hlstd{)}\hlopt{+}
  \hlkwd{ggtitle}\hlstd{(}\hlstr{"too steep"}\hlstd{)}
\end{alltt}
\end{kframe}

{\centering \includegraphics[width=5in,height=4in]{figure/unnamed-chunk-8-1} 

}



\end{knitrout}

How do we measure the distance from each point to whatever line we choose?
For linear regression, the underlying mathematics says that the so-called
$L_2$ norm is quite good (it coincides with the maximum likelihood line --- well beyond the scope of this course). For any given observed $y[i]$ at a given point $x[i,1]$,
$L_2$ norm ({\em penalty}) is 
\[ \left( y[i] - (\Theta_0 + \Theta_1 \cdot x[i,1]) \right)^2 \]
where $\Theta_0$ and $\Theta_1$ is our chosen line.
We compute this over all data points $x[i,1]$ and $y[i]$, 
$1 \leq i \leq N$.

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{sp} \hlkwb{<-} \hlkwd{ggplot}\hlstd{(}\hlkwc{data}\hlstd{=df,} \hlkwd{aes}\hlstd{(}\hlkwc{x}\hlstd{=gene,} \hlkwc{y}\hlstd{=time))} \hlopt{+} \hlkwd{geom_point}\hlstd{()}
\hlstd{sp} \hlopt{+} \hlkwd{geom_abline}\hlstd{(}\hlkwc{intercept} \hlstd{=} \hlnum{30}\hlstd{,} \hlkwc{slope} \hlstd{=} \hlnum{30}\hlstd{,} \hlkwc{color}\hlstd{=}\hlstr{"red"}\hlstd{,} \hlkwc{size} \hlstd{=} \hlnum{1}\hlstd{)}\hlopt{+}
  \hlkwd{ggtitle}\hlstd{(}\hlstr{"ok?"}\hlstd{)}
\end{alltt}
\end{kframe}

{\centering \includegraphics[width=5in,height=4in]{figure/unnamed-chunk-9-1} 

}



\end{knitrout}

\subsection{Finding the Optimal Linear Model}

So how do we find the optimal line? That is, the line that minimizes the $L_2$ norm over all data points $X[1,1], \ldots, X[N,1]$.
Mathematically, we want to fine the line (defined by parameters
$\Theta_0$ and $\Theta_1$) that minimizes the following 
equation:
\[
\left( \sum_{i = 1}^{N} \left( y[i] - (\Theta_0 + \Theta_1 \cdot x[i,1])  \right)^2
\right)^{\frac{1}{2}}
\]
]

In R, this can be found easily using the {\tt lm() function} (linear model).
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{my.line} \hlkwb{<-} \hlkwd{lm}\hlstd{( time} \hlopt{~} \hlstd{gene,}  \hlkwc{data} \hlstd{= df )}
\hlkwd{print}\hlstd{(my.line)}
\end{alltt}
\begin{verbatim}
## 
## Call:
## lm(formula = time ~ gene, data = df)
## 
## Coefficients:
## (Intercept)         gene  
##       31.06        27.39
\end{verbatim}
\begin{alltt}
\hlkwd{summary}\hlstd{(my.line)}
\end{alltt}
\begin{verbatim}
## 
## Call:
## lm(formula = time ~ gene, data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -23.788  -7.783  -2.001   7.374  31.642 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)   31.061      2.058  15.094  < 2e-16 ***
## gene          27.390      6.188   4.426 7.51e-05 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 13.11 on 39 degrees of freedom
## Multiple R-squared:  0.3344,	Adjusted R-squared:  0.3173 
## F-statistic: 19.59 on 1 and 39 DF,  p-value: 7.506e-05
\end{verbatim}
\end{kframe}
\end{knitrout}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{sp} \hlkwb{<-} \hlkwd{ggplot}\hlstd{(}\hlkwc{data}\hlstd{=df,} \hlkwd{aes}\hlstd{(}\hlkwc{x}\hlstd{=gene,} \hlkwc{y}\hlstd{=time))} \hlopt{+} \hlkwd{geom_point}\hlstd{()}
\hlstd{sp} \hlopt{+} \hlkwd{geom_abline}\hlstd{(}\hlkwc{intercept} \hlstd{=} \hlnum{30}\hlstd{,} \hlkwc{slope} \hlstd{=} \hlnum{30}\hlstd{,} \hlkwc{color}\hlstd{=}\hlstr{"red"}\hlstd{,} \hlkwc{size} \hlstd{=} \hlnum{1}\hlstd{)} \hlopt{+}
  \hlkwd{geom_abline}\hlstd{(}\hlkwc{intercept} \hlstd{=} \hlnum{31.06}\hlstd{,} \hlkwc{slope} \hlstd{=} \hlnum{27.39}\hlstd{,} \hlkwc{color}\hlstd{=}\hlstr{"blue"}\hlstd{,} \hlkwc{size} \hlstd{=} \hlnum{1}\hlstd{)} \hlopt{+}
  \hlkwd{ggtitle}\hlstd{(}\hlstr{"We are pretty good!"}\hlstd{)}
\end{alltt}
\end{kframe}

{\centering \includegraphics[width=5in,height=4in]{figure/unnamed-chunk-11-1} 

}



\end{knitrout}

\subsection{Using our model to predict new samples}

We started out by removing all patients that did not recur but also
all patients that recurred after $5$ years.
Let's try out our model to see if it predicts such ``late recurrers'.

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{late_recur} \hlkwb{<-} \hlkwd{intersect}\hlstd{(}
        \hlkwd{which}\hlstd{(tcga}\hlopt{$}\hlstd{clinical}\hlopt{$}\hlstd{event}\hlopt{==}\hlnum{TRUE}\hlstd{),}
        \hlkwd{which}\hlstd{(tcga}\hlopt{$}\hlstd{clinical}\hlopt{$}\hlstd{event.5}\hlopt{==}\hlnum{FALSE}\hlstd{))}
\hlstd{lclinical} \hlkwb{<-} \hlstd{tcga}\hlopt{$}\hlstd{clinical[late_recur, ]}
\hlkwd{nrow}\hlstd{(lclinical)}
\end{alltt}
\begin{verbatim}
## [1] 24
\end{verbatim}
\begin{alltt}
\hlstd{lclinical[,} \hlkwd{c}\hlstd{(}\hlnum{1}\hlstd{,} \hlnum{11}\hlopt{:}\hlnum{14} \hlstd{)]}
\end{alltt}
\begin{verbatim}
##                id      time event time.5 event.5
## tcga.10   tcga.10  81.63288  TRUE     60   FALSE
## tcga.103 tcga.103  63.12329  TRUE     60   FALSE
## tcga.147 tcga.147 129.56712  TRUE     60   FALSE
## tcga.168 tcga.168  65.52329  TRUE     60   FALSE
## tcga.169 tcga.169 146.49863  TRUE     60   FALSE
## tcga.196 tcga.196  84.59178  TRUE     60   FALSE
## tcga.198 tcga.198  68.94247  TRUE     60   FALSE
## tcga.201 tcga.201  83.86849  TRUE     60   FALSE
## tcga.202 tcga.202  79.46301  TRUE     60   FALSE
## tcga.217 tcga.217 113.78630  TRUE     60   FALSE
## tcga.269 tcga.269  78.01644  TRUE     60   FALSE
## tcga.279 tcga.279  81.17260  TRUE     60   FALSE
## tcga.297 tcga.297 102.77260  TRUE     60   FALSE
## tcga.302 tcga.302 100.70137  TRUE     60   FALSE
## tcga.405 tcga.405 112.37260  TRUE     60   FALSE
## tcga.418 tcga.418  97.47945  TRUE     60   FALSE
## tcga.429 tcga.429 140.28493  TRUE     60   FALSE
## tcga.430 tcga.430 129.69863  TRUE     60   FALSE
## tcga.317 tcga.317  66.04932  TRUE     60   FALSE
## tcga.322 tcga.322  90.83836  TRUE     60   FALSE
## tcga.332 tcga.332  72.55890  TRUE     60   FALSE
## tcga.518 tcga.518 113.81918  TRUE     60   FALSE
## tcga.528 tcga.528  82.84932  TRUE     60   FALSE
## tcga.533 tcga.533  91.98904  TRUE     60   FALSE
\end{verbatim}
\end{kframe}
\end{knitrout}
Now remove the corresponding columns from the expression matrix
and visualize.
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{lexprs} \hlkwb{<-} \hlstd{tcga}\hlopt{$}\hlstd{exprs[ , late_recur]}
\hlkwd{dim}\hlstd{(lexprs)}
\end{alltt}
\begin{verbatim}
## [1] 90797    24
\end{verbatim}
\begin{alltt}
\hlstd{ldf} \hlkwb{<-} \hlkwd{data.frame}\hlstd{(}  \hlkwc{gene} \hlstd{= lexprs[klf15, ],} \hlkwc{time} \hlstd{= lclinical}\hlopt{$}\hlstd{time)}
\end{alltt}
\end{kframe}
\end{knitrout}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{qplot}\hlstd{(gene, time,}  \hlkwc{data}\hlstd{=ldf,}\hlkwc{geom} \hlstd{=} \hlkwd{c}\hlstd{(}\hlstr{"point"}\hlstd{,} \hlstr{"smooth"}\hlstd{))}
\end{alltt}


{\ttfamily\noindent\itshape\color{messagecolor}{\#\# `geom\_smooth()` using method = 'loess' and formula 'y \textasciitilde{} x'}}\end{kframe}

{\centering \includegraphics[width=5in,height=4in]{figure/unnamed-chunk-14-1} 

}



\end{knitrout}
Hhhhmmmm. Let's try a linear model anyway.
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{late.line} \hlkwb{<-} \hlkwd{lm}\hlstd{( time} \hlopt{~} \hlstd{gene,}  \hlkwc{data} \hlstd{= ldf )}
\hlkwd{print}\hlstd{(late.line)}
\end{alltt}
\begin{verbatim}
## 
## Call:
## lm(formula = time ~ gene, data = ldf)
## 
## Coefficients:
## (Intercept)         gene  
##      94.787        4.014
\end{verbatim}
\begin{alltt}
\hlkwd{summary}\hlstd{(late.line)}
\end{alltt}
\begin{verbatim}
## 
## Call:
## lm(formula = time ~ gene, data = ldf)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -29.432 -15.650  -7.734  16.837  53.249 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)   94.787      5.056  18.747 5.13e-15 ***
## gene           4.014     16.457   0.244     0.81    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 24.67 on 22 degrees of freedom
## Multiple R-squared:  0.002697,	Adjusted R-squared:  -0.04263 
## F-statistic: 0.0595 on 1 and 22 DF,  p-value: 0.8095
\end{verbatim}
\end{kframe}
\end{knitrout}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{sp} \hlkwb{<-} \hlkwd{ggplot}\hlstd{(}\hlkwc{data}\hlstd{=ldf,} \hlkwd{aes}\hlstd{(}\hlkwc{x}\hlstd{=gene,} \hlkwc{y}\hlstd{=time))} \hlopt{+} \hlkwd{geom_point}\hlstd{()}
\hlstd{sp} \hlopt{+} \hlkwd{geom_abline}\hlstd{(}\hlkwc{intercept} \hlstd{=} \hlnum{94.787}\hlstd{,} \hlkwc{slope} \hlstd{=} \hlnum{4.014}\hlstd{,} \hlkwc{color}\hlstd{=}\hlstr{"green"}\hlstd{,} \hlkwc{size} \hlstd{=} \hlnum{1}\hlstd{)} \hlopt{+}
  \hlkwd{ggtitle}\hlstd{(}\hlstr{"Sucky"}\hlstd{)}
\end{alltt}
\end{kframe}

{\centering \includegraphics[width=5in,height=4in]{figure/unnamed-chunk-16-1} 

}



\end{knitrout}

Let's first plot everything together for clarity.
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{ldf}\hlopt{$}\hlstd{kind} \hlkwb{<-} \hlstr{"late"}\hlstd{; df}\hlopt{$}\hlstd{kind} \hlkwb{<-} \hlstr{"early"}\hlstd{; tot.df} \hlkwb{<-} \hlkwd{rbind}\hlstd{(ldf, df)}
\hlstd{sp} \hlkwb{<-} \hlkwd{ggplot}\hlstd{(}\hlkwc{data}\hlstd{=tot.df,} \hlkwd{aes}\hlstd{(}\hlkwc{x}\hlstd{=gene,} \hlkwc{y}\hlstd{=time))} \hlopt{+} \hlkwd{geom_point}\hlstd{(}\hlkwd{aes}\hlstd{(}\hlkwc{color}\hlstd{=}\hlkwd{factor}\hlstd{(tot.df}\hlopt{$}\hlstd{kind)))}
\hlstd{sp} \hlopt{+}  \hlkwd{geom_abline}\hlstd{(}\hlkwc{intercept} \hlstd{=} \hlnum{31.06}\hlstd{,} \hlkwc{slope} \hlstd{=} \hlnum{27.39}\hlstd{,} \hlkwc{color}\hlstd{=}\hlstr{"blue"}\hlstd{,} \hlkwc{size} \hlstd{=} \hlnum{1}\hlstd{)} \hlopt{+}
  \hlkwd{ggtitle}\hlstd{(}\hlstr{"Everything"}\hlstd{)}
\end{alltt}
\end{kframe}

{\centering \includegraphics[width=5in,height=4in]{figure/unnamed-chunk-17-1} 

}



\end{knitrout}
In fact, it is believed that the molecular factors that underlie late recurrence are much different than the factors that underlie 
early recurrence.


\section{Multiple Linear Regression}

Often we would like to combine several attributes to predict the level of 
$y$.
Instead of just KLF15, we would like to include other genes in our 
prediction of outcome. 
In total, we might have $D$ such genes.
Now we have an equation as follows:
$$ y[i] \sim \Theta_0 + \Theta_1 \cdot x[i,1] + \Theta_2 \cdot x[i,2] + 
\ldots + \Theta_D \cdot x[i,D] + \epsilon.$$
So there are now $D+1$ parameters (don't forget the intercept $\Theta_0$).

For simplicity, let's consider $D=2$ and build upon our analysis of KLF15
Here we will add a second gene called HES5 (Hairy and Enhancer of Split5), also a transcription factor implicated in NOTCH signalling
{\tt https://www.genecards.org/cgi-bin/carddisp.pl?gene=HES5}.
We are going to build a linear model of the form
\[
time \sim \Theta_0 + \Theta_1 \cdot KLF15 + \Theta_2 \cdot HES5 + \epsilon.
\]
That is, now our time to recurrence is a function of a {\em weighted} amount of KLF15 expression and a {\em weighted} amount of HES5 expression (along with some noise $\epsilon$ as before).

As before ...
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{rclinical} \hlkwb{<-} \hlstd{tcga}\hlopt{$}\hlstd{clinical[}\hlkwd{which}\hlstd{(tcga}\hlopt{$}\hlstd{clinical}\hlopt{$}\hlstd{event.5}\hlopt{==}\hlnum{TRUE}\hlstd{), ]}
\hlstd{rexprs} \hlkwb{<-} \hlstd{tcga}\hlopt{$}\hlstd{exprs[ ,} \hlkwd{which}\hlstd{(tcga}\hlopt{$}\hlstd{clinical}\hlopt{$}\hlstd{event.5)]}
\end{alltt}
\end{kframe}
\end{knitrout}
Now take the first transcript for both genes:
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{klf15_ind} \hlkwb{<-} \hlkwd{which}\hlstd{(tcga}\hlopt{$}\hlstd{probe.info}\hlopt{$}\hlstd{gene.name} \hlopt{==} \hlstr{"KLF15"}\hlstd{)[}\hlnum{1}\hlstd{]}
\hlstd{hes5_ind} \hlkwb{<-} \hlkwd{which}\hlstd{(tcga}\hlopt{$}\hlstd{probe.info}\hlopt{$}\hlstd{gene.name} \hlopt{==} \hlstr{"HES5"}\hlstd{)[}\hlnum{1}\hlstd{]}
\hlstd{gene.df} \hlkwb{<-} \hlkwd{data.frame}\hlstd{(}  \hlkwc{klf15}\hlstd{= rexprs[klf15_ind, ],}
                        \hlkwc{hes5} \hlstd{= rexprs[hes5_ind,],}
                        \hlkwc{time} \hlstd{= rclinical}\hlopt{$}\hlstd{time.5)}
\hlstd{linearmodel} \hlkwb{<-} \hlkwd{lm}\hlstd{( time} \hlopt{~} \hlstd{klf15} \hlopt{+} \hlstd{hes5,} \hlkwc{data} \hlstd{= gene.df)}
\hlkwd{print}\hlstd{(linearmodel)}
\end{alltt}
\begin{verbatim}
## 
## Call:
## lm(formula = time ~ klf15 + hes5, data = gene.df)
## 
## Coefficients:
## (Intercept)        klf15         hes5  
##      45.911       25.862       -6.895
\end{verbatim}
\begin{alltt}
\hlkwd{summary}\hlstd{(linearmodel)}
\end{alltt}
\begin{verbatim}
## 
## Call:
## lm(formula = time ~ klf15 + hes5, data = gene.df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -23.462  -7.128  -2.255   5.999  32.003 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)   45.911      3.668  12.517 4.71e-15 ***
## klf15         25.862      5.054   5.117 9.21e-06 ***
## hes5          -6.895      1.515  -4.552 5.32e-05 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 10.68 on 38 degrees of freedom
## Multiple R-squared:  0.5693,	Adjusted R-squared:  0.5466 
## F-statistic: 25.11 on 2 and 38 DF,  p-value: 1.122e-07
\end{verbatim}
\end{kframe}
\end{knitrout}

The package {\tt gg3D} is nice for drawing 3D plots in R.
It builds upon {\tt ggplot2}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlcom{#install.packages("devtools"); }
\hlkwd{library}\hlstd{(devtools)}
\end{alltt}


{\ttfamily\noindent\itshape\color{messagecolor}{\#\# Loading required package: usethis}}\begin{alltt}
\hlstd{devtools}\hlopt{::}\hlkwd{install_github}\hlstd{(}\hlstr{"AckerDWM/gg3D"}\hlstd{)}
\end{alltt}


{\ttfamily\noindent\itshape\color{messagecolor}{\#\# Skipping install of 'gg3D' from a github remote, the SHA1 (ffdd837d) has not changed since last install.\\\#\#\ \  Use `force = TRUE` to force installation}}\begin{alltt}
\hlkwd{library}\hlstd{(}\hlstr{"gg3D"}\hlstd{)}

\hlkwd{ggplot}\hlstd{(gene.df,} \hlkwd{aes}\hlstd{(}\hlkwc{x}\hlstd{=klf15,} \hlkwc{y}\hlstd{=hes5,} \hlkwc{z}\hlstd{=time))} \hlopt{+}
  \hlkwd{theme_void}\hlstd{()} \hlopt{+}
  \hlkwd{axes_3D}\hlstd{()} \hlopt{+}
  \hlkwd{stat_3D}\hlstd{(}\hlkwd{aes}\hlstd{(}\hlkwc{color}\hlstd{=time),} \hlkwc{alpha}\hlstd{=}\hlnum{1}\hlstd{)} \hlopt{+}
  \hlkwd{labs_3D}\hlstd{(}
    \hlkwc{labs}\hlstd{=}\hlkwd{c}\hlstd{(}\hlstr{"KLF15"}\hlstd{,} \hlstr{"HES5"}\hlstd{,} \hlstr{"Time to Recurrence"}\hlstd{),}
    \hlkwc{hjust}\hlstd{=}\hlkwd{c}\hlstd{(}\hlnum{0}\hlstd{,}\hlnum{1}\hlstd{,}\hlnum{1}\hlstd{),} \hlkwc{vjust}\hlstd{=}\hlkwd{c}\hlstd{(}\hlnum{1}\hlstd{,} \hlnum{1}\hlstd{,} \hlopt{-}\hlnum{0.2}\hlstd{),} \hlkwc{angle}\hlstd{=}\hlkwd{c}\hlstd{(}\hlnum{0}\hlstd{,} \hlnum{0}\hlstd{,} \hlnum{90}\hlstd{))} \hlopt{+}
  \hlkwd{geom_abline}\hlstd{(}\hlkwc{intercept} \hlstd{=} \hlnum{31.06}\hlstd{,} \hlkwc{slope} \hlstd{=} \hlnum{27.39}\hlstd{,} \hlkwc{color}\hlstd{=}\hlstr{"blue"}\hlstd{,} \hlkwc{size} \hlstd{=} \hlnum{1}\hlstd{)} \hlopt{+}
  \hlkwd{theme_void}\hlstd{()}
\end{alltt}
\end{kframe}
\includegraphics[width=\maxwidth]{figure/unnamed-chunk-20-1} 
\begin{kframe}\begin{alltt}
\hlcom{#install.packages("rgl")}
\hlkwd{library}\hlstd{(rgl)}
\hlkwd{plot3d}\hlstd{(gene.df}\hlopt{$}\hlstd{klf15, gene.df}\hlopt{$}\hlstd{hes5, gene.df}\hlopt{$}\hlstd{time,}
        \hlkwc{size}\hlstd{=}\hlnum{3}\hlstd{,} \hlkwc{type}\hlstd{=}\hlstr{"s"}\hlstd{,}
      \hlcom{# xlim=c(-2,2), ylim=c(-2,2),zlim=c(-2,2),}
      \hlkwc{col}\hlstd{=}\hlstr{"red"}\hlstd{,}
       \hlkwc{xlab}\hlstd{=}\hlstr{"kfl15"}\hlstd{,}\hlkwc{ylab}\hlstd{=}\hlstr{"hes5"}\hlstd{,}\hlkwc{zlab}\hlstd{=}\hlstr{"tim"}\hlstd{)}
\end{alltt}
\end{kframe}
\end{knitrout}



\end{document}
